#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

static const int d = 100;
static const Rect2i rc_cell{0, 0, d, d};

void draw_cell(Mat& img, const int32_t x, const int32_t y, 
  const Scalar color_ground, const Scalar color_figure) {
  Rect2i rc = rc_cell;
  rc.x += x;
  rc.y += y;
  rectangle(img, rc, color_ground, -1);
  circle(img, {rc.x + rc.width / 2, rc.y + rc.height / 2}, rc.width / 5, color_figure, -1);
}

int main() {
  Mat img_src(200, 300, CV_8UC1);

  draw_cell(img_src, 0, 0, { 255 }, { 127 });
  draw_cell(img_src, d, 0, { 127 }, { 0 });
  draw_cell(img_src, d * 2, 0, { 0 }, { 255 });
  draw_cell(img_src, 0, d, { 0 }, { 127 });
  draw_cell(img_src, d, d, { 255 }, { 0 });
  draw_cell(img_src, d * 2, d, { 127 }, { 255 });

  Mat srcf;
  img_src.convertTo(srcf, CV_32FC1);
  Mat res1f(200, 300, CV_32FC1);
  Mat res2f(200, 300, CV_32FC1);
  Mat res12f(200, 300, CV_32FC1);
  Mat res1(200, 300, CV_8UC1);
  Mat res2(200, 300, CV_8UC1);
  Mat res12(200, 300, CV_8UC1);

  int ddepth = -1;
  float matrix1[] = { 1.0f, 0.0f, 0.0f, -1.0f };
  float matrix2[] = { 1.0f, 0.0f, -1.0f, 2.0f, 0.0f, -2.0f, 1.0f, 0.0f, -1.0f };
  Mat kernel1(2, 2, CV_32FC1, (float*)matrix1);
  Mat kernel2(3, 3, CV_32FC1, (float*)matrix2);

  filter2D(srcf, res1f, ddepth, kernel1);
  filter2D(srcf, res2f, ddepth, kernel2);
  for (int i = 0; i < 200; i++)
  {
	  for (int j = 0; j < 300; j++)
	  {
		  res12f.at<float>(i, j) = (sqrt(res1f.at<float>(i, j) * res1f.at<float>(i, j) + res2f.at<float>(i, j) * res2f.at<float>(i, j)));
	  }
  }

  Mat res(200, 300, CV_8UC1);
  res1f.convertTo(res1, CV_8UC1);
  res2f.convertTo(res2, CV_8UC1);
  res12f.convertTo(res12, CV_8UC1);
  srcf.convertTo(res, CV_8UC1);
  imwrite("../../../build/prj.lab/lab04/lab04_src.jpg", res);
  imwrite("../../../build/prj.lab/lab04/lab04_f1.jpg", res1);
  imwrite("../../../build/prj.lab/lab04/lab04_f2.jpg", res2);
  imwrite("../../../build/prj.lab/lab04/lab04_f12.jpg", res12);

  hconcat(res, res1, res);
  hconcat(res2, res12, res2);
  vconcat(res, res2, res);

  imshow("lab04", res);

  waitKey(0);
  return 0;
}