#include <opencv2/opencv.hpp>
using namespace cv;
int main() {
  Mat img3(60, 768, CV_8UC1);
  Mat img30(60, 768, CV_8UC1);
  img3 = 0;
  img30 = 0;

  Rect2d rc3 = {0, 0, 3, 60 };
  for (double i = 0; i <= 255; i++)
  {
	  rectangle(img3, rc3, { i }, -1);
	  rc3.x += rc3.width;
  }

  Rect2d rc30 = { 0, 0, 30, 60 };
  for (double i = 5; i <= 255; i = i + 10)
  {
	  rectangle(img30, rc30, { i }, -1);
	  rc30.x += rc30.width;
  }

  Mat gam3 = img3.clone();
  Mat gam30 = img30.clone();
  Mat lut_matrix(1, 256, CV_8UC1);
  uchar * ptr = lut_matrix.ptr();
  for (int i = 0; i < 256; i++)
	  ptr[i] = (int)(pow((double)i / 255.0, 2.3) * 255.0);
  Mat result3;
  Mat result30;

  LUT(gam3, lut_matrix, result3);
  LUT(gam30, lut_matrix, result30);

  vconcat(img3, result3, img3);
  vconcat(img30, result30, img30);
  vconcat(img3, img30, img3);
  imwrite("lab01.png", img3);
  imshow("lab01", img3);
  waitKey(0);
  return 0;
}
