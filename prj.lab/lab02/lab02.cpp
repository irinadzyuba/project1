#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

int main() {
  Mat img = imread("../../../testdata/gg.jpeg", IMREAD_COLOR);

  vector<int> parameters;
  parameters.push_back(IMWRITE_JPEG_QUALITY);
  parameters.push_back(90);
  imwrite("../../../build/prj.lab/lab02/gg90.jpg", img, parameters);

  parameters.pop_back();
  parameters.push_back(50);
  imwrite("../../../build/prj.lab/lab02/gg50.jpg", img, parameters);

  Mat img90 = imread("gg90.jpg");
  Mat img50 = imread("gg50.jpg");
  Mat res;
  hconcat(img, img90, res);
  hconcat(res, img50, res);

  Mat difference = (img90 - img50)*10;

  Mat channel_blue[3];
  Mat channel_green[3];
  Mat channel_red[3];

  split(difference, channel_blue);
  channel_blue[1] = Mat::zeros(img.rows, img.cols, CV_8UC1);
  channel_blue[2] = Mat::zeros(img.rows, img.cols, CV_8UC1);

  split(difference, channel_green);
  channel_green[0] = Mat::zeros(img.rows, img.cols, CV_8UC1);
  channel_green[2] = Mat::zeros(img.rows, img.cols, CV_8UC1);

  split(difference, channel_red);
  channel_red[0] = Mat::zeros(img.rows, img.cols, CV_8UC1);
  channel_red[1] = Mat::zeros(img.rows, img.cols, CV_8UC1);

  Mat img1;
  Mat img2;
  Mat img3;
  merge(channel_blue, 3, img1);
  merge(channel_green, 3, img2);
  merge(channel_red, 3, img3);

  hconcat(img1, img2, img1);
  hconcat(img1, img3, img1);
  vconcat(res, img1, res);
  imwrite("../../../build/prj.lab/lab02/lab02.jpg", res);
  imshow("lab02", res);

  waitKey(0);
  return 0;

}