#include <opencv2/opencv.hpp>


using namespace cv;
using namespace std;

float range[] = { 0, 256 };
const float* histRange = { range };

Mat Histogram(Mat& img)
{
	Mat gray[1];
	split(img, gray);
	int histSize = 256;
	Mat w_hist;
	calcHist(&gray[0], 1, 0, Mat(), w_hist, 1, &histSize, &histRange, true, false);
	int hist_w = 600, hist_h = 400;
	int bin_w = cvRound((double)hist_w / histSize);
	Mat hist1(hist_h, hist_w, CV_8UC1, Scalar(0, 0, 0));
	normalize(w_hist, w_hist, 0, hist1.rows, NORM_MINMAX, -1, Mat());
	for (int i = 1; i < histSize; i++)
	{
		line(hist1, Point(bin_w*(i - 1), hist_h - cvRound(w_hist.at<float>(i - 1))), Point(bin_w*(i), hist_h - cvRound(w_hist.at<float>(i))), Scalar(255, 255, 255), 2, 8, 0);
	}
	return hist1;
}

int main() {
  Mat img = imread("../../../testdata/eye.jpg", ImreadModes::IMREAD_GRAYSCALE);

  Mat hist1 = Histogram(img);
  imwrite("lab03.hist.src.png", hist1);

  Ptr<CLAHE> clahe1 = createCLAHE();
  clahe1->setTilesGridSize(Size(3, 3));
  Mat clah1;
  clahe1->apply(img, clah1);
  imwrite("lab03.clahe.1.png", clah1);
  Mat clahist1 = Histogram(clah1);
  imwrite("lab03.hist.clahe.1.png", clahist1);

  Ptr<CLAHE> clahe2 = createCLAHE();
  clahe2->setTilesGridSize(Size(10, 10));
  Mat clah2;
  clahe2->apply(img, clah2);
  imwrite("lab03.clahe.2.png", clah2);
  Mat clahist2 = Histogram(clah2);
  imwrite("lab03.hist.clahe.2.png", clahist2);

  Ptr<CLAHE> clahe3 = createCLAHE();
  clahe3->setTilesGridSize(Size(25, 25));
  Mat clah3;
  clahe3->apply(img, clah3);
  imwrite("lab03.clahe.3.png", clah3);
  Mat clahist3 = Histogram(clah3);
  imwrite("lab03.hist.clahe.3.png", clahist3);

  Mat otsu;
  int threshold_value = 100;
  threshold(img, otsu, threshold_value, 255, THRESH_BINARY);
  hconcat(img, otsu, otsu);
  imwrite("lab03.bin.global.png", otsu);
  
  Mat local;
  adaptiveThreshold(img, local, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 5, 0);
  hconcat(img, local, local);
  imwrite("lab03.bin.local.png", local);
}